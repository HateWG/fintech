//
//  SecondViewController.swift
//  retain-cycle
//
//  Created by Томас Димеджи Акинделе Ало on 14/10/2018.
//  Copyright © 2018 Tinkoff Fintech. All rights reserved.
//

import Foundation
import UIKit

class SecondViewController: UIViewController {
    
        
    var aBlock: (() -> ()) = { }
    let aConstant = 5
 
    var closure : (() -> ()) = { }
    var counter = 0
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("init")
        
        aBlock = {
            [weak self] in
            guard let self = self else { return }
            print(self.aConstant)
        }
        
        closure = {
            [weak self] in
            guard let self = self else { return }
            self.counter += 1
            print(self.counter)
        }
        
        func foo() {
            closure()
        }
    }
    
    deinit {
        print("deinit")
    }

}
