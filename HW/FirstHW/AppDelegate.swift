//
//  AppDelegate.swift
//  FirstHW
//
//  Created by Томас Димеджи Акинделе Ало on 19.09.2018.
//  Copyright © 2018 Tinkoff Fintech. All rights reserved.
//

import UIKit



@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate{
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        print("Application moved from <Not running> to <Inactive>: <\(#function)>")

        self.window = EmittingWindow(frame: UIScreen.main.bounds)
//
        guard let navController: UINavigationController = UIStoryboard(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "navController") as? UINavigationController else {
                preconditionFailure("Wrong root controller loaded from initial storyboard")
        }
        window?.rootViewController = navController
//        window?.makeKeyAndVisible()
//        let gestureRecogniger = UIPanGestureRecognizer(target: self, action: #selector(self.handlePan(sender:))) // Implement your selector
//        gestureRecogniger.cancelsTouchesInView = false
//        window?.addGestureRecognizer(gestureRecogniger)
//
//        gestureRecogniger.delegate = self
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        print("Application moved from <Active> to <Inactive>: <\(#function)>")
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        
        print("Application moved from <Inactive> to <Background>: <\(#function)>")
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        
        print("Application moved from <Background> to <Foreground>: <\(#function)>")
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        print("Application moved from <Inactive> to <Active>: <D\(#function)>")
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        //       Система перемещает приложение в состояние "Suspended" из состояния "Background"  автоматически и не предупреждает об этом.
        
        print("Application moved from <Suspended> to <Not running>: <D\(#function)>")
    }

}

//extension AppDelegate: UIGestureRecognizerDelegate {
//    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
//        return true
//    }
//
//    @objc func handlePan(sender: UIPanGestureRecognizer) {
//
//        switch sender.state {
//
//        case .began:
//
//            print("!!!start and began")
//
//        case .changed:
//            print("!!!!change")
//
//        case .cancelled, .ended:
//            print("!!!!Ennnd")
//
//        default:
//            break
//        }
//    }
//}
