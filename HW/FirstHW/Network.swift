//
//  Network.swift
//  FirstHW
//
//  Created by Томас Димеджи Акинделе Ало on 21/11/2018.
//  Copyright © 2018 Tinkoff Fintech. All rights reserved.
//

import Foundation
import UIKit

enum Result<T> {
    case success(T)
    case error(String)
}

protocol NetworkServiceProtocol: class {
    func load(completion: @escaping ([URL]) -> Void)
    func loadImage(url: URL, completion: @escaping (UIImage?) -> Void, fault: @escaping () -> ()) -> URLSessionTask
}

class NetworkService: NetworkServiceProtocol {
    
    let session = URLSession.shared
//    let apiKey = "10812835-4160f0bd49d231f1f3f750437"
    let baseURL = "https://pixabay.com/api/"
    let apiKey = "10812835-4160f0bd49d231f1f3f750437"
    let category = "cat"
    let imageType = "photo"
    let pretty = true
    let perPage = 50
    var listUrl: URL?
    
    init() {
        let path = baseURL + "?key=" + apiKey + "&q=" + category + "&image_type="+imageType + "&pretty=\(pretty)" + "&per_page=\(perPage)"
        self.listUrl = URL(string: path)
    }
    
    func parse(data: Data) -> [URL]? {
        do {
            
            guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject] else {
                return nil
            }
            let hits = json["hits"] as! [[String: AnyObject]]
            let imageURLs = hits
                .map { $0["webformatURL"] as! String }
                .map { URL(string: $0)! }
            
            return imageURLs
            
        } catch {
            print("Can't get data from JSON\n\(error)")
            return nil
        }
    }
    
    
    func load (completion: @escaping ([URL]) -> Void) {
        
        let url = listUrl!
        let request = URLRequest(url: url)
        
        let task = session.dataTask(with: request, completionHandler: { data, response, error in
            guard let data = data, let imageURLs = self.parse(data: data) else { return }
//            let imageURLs = self.parse(data: data)
//            let json = try! JSONSerialization.jsonObject(with: data) as! [String: AnyObject]
//            let hits = json["hits"] as! [[String: AnyObject]]
//            let imageURLs = hits
//                .map { $0["webformatURL"] as! String }
//                .map { URL(string: $0)! }

        DispatchQueue.main.async {
            completion(imageURLs)
        }
    })
        task.resume()
    }
    
    
    
    
    func loadImage(url: URL, completion: @escaping (UIImage?) -> Void, fault: @escaping () -> ()) -> URLSessionTask {
    let request = URLRequest(url: url)
        
    let session = URLSession.shared
    let task = session.dataTask(with: request, completionHandler: { data, response, error in
        guard let data = data else {
            if error != nil {
                fault()
            }
            return
        }
        guard let image = UIImage(data: data) else {
            fault()
            return
        }
        
        DispatchQueue.main.async {
            completion(image)
        }
    })
    task.resume()
    return task
    
}
    
}
