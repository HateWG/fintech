//
//  Date.swift
//  FirstHW
//
//  Created by Томас Димеджи Акинделе Ало on 30/10/2018.
//  Copyright © 2018 Tinkoff Fintech. All rights reserved.
//

import Foundation


extension Date {
    
    static func dateFromCustomString(dateString: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        return dateFormatter.date(from: dateString) ?? Date()
    }
    
    static func timeFromCustomString(dateString: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        return dateFormatter.date(from: dateString) ?? Date()
    }
    
    func reduceToMonthDayYear() -> Date {
        let calendar = Calendar.current
        let month = calendar.component(.month, from: self)
        let day = calendar.component(.day, from: self)
        let year = calendar.component(.year, from: self)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        return dateFormatter.date(from: "\(day)/\(month)/\(year)") ?? Date()
    }
    
    static func currentDate() -> Date {
        let CurrentDateFormatter = DateFormatter()
        CurrentDateFormatter.dateFormat = "dd/MM/yyyy"
        
        let date = NSDate()
        let calendar = Calendar.current
        let currentMonth = calendar.component(.month, from: date as Date)
        let currentDay = calendar.component(.day, from: date as Date)
        let currentYear = calendar.component(.year, from: date as Date)
        let currentDate = CurrentDateFormatter.date(from: "\(currentDay)/\(currentMonth)/\(currentYear)") ?? Date()
        return currentDate
    }
    
}
