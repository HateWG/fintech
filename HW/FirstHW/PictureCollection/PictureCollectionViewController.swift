//
//  PictureCollectionViewController.swift
//  FirstHW
//
//  Created by Томас Димеджи Акинделе Ало on 26/11/2018.
//  Copyright © 2018 Tinkoff Fintech. All rights reserved.
//

import Foundation
import UIKit

protocol PictureLoaderViewControllerDelegate: class {
    func imagePicked(image: UIImage)
}

class PictureCollectionViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    
    private var manager: NetworkService?
    
    weak var delegate: PictureLoaderViewControllerDelegate?
    
    var parsedImages = [URL : UIImage]()
    var imagesLinks = [URL]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        spinner.startAnimating()
        collectionView.delegate = self
        collectionView.dataSource = self
        spinner.hidesWhenStopped = true
        manager = NetworkService.init()
        loadImagesList()
        
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(returnToProfileViewController))
        
    }
    
    @objc func returnToProfileViewController() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelButtonAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        print(imagesLinks)
    }
    

    private func loadImagesList() {
        
        manager?.load(completion: { images in

            for image in images {
                self.imagesLinks.append(image)
                self.collectionView.reloadData()
                self.spinner.stopAnimating()
            }
        })
    }

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.imagesLinks.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath) as! PictureCollectionViewCustomCell
        
//        cell.imageView.image = UIImage.init(named: "placeholderuser")
        
        DispatchQueue.main.async {
            cell.imageView.image = UIImage.init(named: "placeholderuser")
        }

        DispatchQueue.main.async {
            let url = self.imagesLinks[indexPath.row]
            cell.imageView.image = self.parsedImages[url]
            print(url)
            
            
        }
        DispatchQueue.main.async {
//            let indexPath = IndexPath(row: indexPath.row, section: 0)
//            collectionView.reloadItems(at: [indexPath])
            print(indexPath.row)
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
//        loadImages()
        DispatchQueue.main.async {
            let url = self.imagesLinks[indexPath.row]
            self.manager?.loadImage(url: url, completion:  {image in
                self.parsedImages[url] = image
            }, fault: {
                                print("Something going wrong with converting URL to image")
            }
            )
        }
//        let url = self.imagesLinks[indexPath.row]
//        self.manager?.loadImage(url: url, completion:  {image in
//////
//            self.parsedImages[url] = image
////
//        }, fault: {
//////            DispatchQueue.main.async {
////                print("Something going wrong with converting URL to image")
////            }
//        }
//        )
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        print("ne pokagy")
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let selectedCell = collectionView.cellForItem(at: indexPath) as? PictureCollectionViewCustomCell,
            let image = selectedCell.imageView.image else {
                return
        }
        print(image.description)
        
        delegate?.imagePicked(image: image)
        dismiss(animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = ((collectionView.bounds.width) - 2 * 20) / 3
        
        return CGSize(width: size, height: size)
    }
}
