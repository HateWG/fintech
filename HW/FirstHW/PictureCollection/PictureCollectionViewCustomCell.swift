//
//  PictureCollectionViewCustomCell.swift
//  FirstHW
//
//  Created by Томас Димеджи Акинделе Ало on 27/11/2018.
//  Copyright © 2018 Tinkoff Fintech. All rights reserved.
//

import Foundation
import UIKit

class PictureCollectionViewCustomCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    override var isSelected: Bool {
        didSet {
            if self.isSelected {
                self.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
                self.contentView.backgroundColor = UIColor.red
            }
            else {
                self.transform = CGAffineTransform.identity
                self.contentView.backgroundColor = UIColor.gray
                
            }
        }
    }
}
