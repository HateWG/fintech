//
//  MessageCellConfiguration.swift
//  FirstHW
//
//  Created by Томас Димеджи Акинделе Ало on 19/11/2018.
//  Copyright © 2018 Tinkoff Fintech. All rights reserved.
//

import Foundation

protocol MessageCellConfiguration: class {
    var text: String { get }
    var isIncoming: Bool { get }
    var date: Date { get }
}

class ChatMessage: MessageCellConfiguration {
    let text: String
    let isIncoming: Bool
    let date: Date
    
    init(text: String, isIncoming: Bool, date: Date) {
        self.text = text
        self.isIncoming = isIncoming
        self.date = date
    }
    
}
