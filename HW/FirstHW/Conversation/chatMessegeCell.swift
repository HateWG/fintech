//
//  ChatMessageCell.swift
//  FirstHW
//
//  Created by Томас Димеджи Акинделе Ало on 08/10/2018.
//  Copyright © 2018 Tinkoff Fintech. All rights reserved.
//

import UIKit



class ChatMessageCell: UITableViewCell {
    
    
    let messageLabel = UILabel()
    let dateLabel = UILabel()
    let customBackgroundView = UIView()
    
    var leadingConstraint: NSLayoutConstraint!
    var trailingConstraint: NSLayoutConstraint!
    
    var dateLeadingConstraint: NSLayoutConstraint!
    var dateTrailingConstraint: NSLayoutConstraint!
    
    var message : Message?
    
    func configCell(message: Message) {
        self.message = message
        messageLabel.text = message.text
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        dateLabel.text = dateFormatter.string(from: message.date!)
        dateLabel.font = UIFont.italicSystemFont(ofSize: 11.0)
        
        if message.isIncoming {
            customBackgroundView.backgroundColor = #colorLiteral(red: 0.9138175845, green: 0.8947939277, blue: 0.5955746174, alpha: 1)
            leadingConstraint.isActive = true
            trailingConstraint.isActive = false
            
            dateLeadingConstraint.isActive = true
            dateTrailingConstraint.isActive = false
        } else {
            customBackgroundView.backgroundColor = #colorLiteral(red: 0.7694644332, green: 0.8320515752, blue: 0.9427327514, alpha: 1)
            leadingConstraint.isActive = false
            trailingConstraint.isActive = true
            
            dateLeadingConstraint.isActive = false
            dateTrailingConstraint.isActive = true
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .clear
        customBackgroundView.backgroundColor = .yellow
        customBackgroundView.layer.cornerRadius = 12
        customBackgroundView.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.numberOfLines = 0
        
        dateLabel.translatesAutoresizingMaskIntoConstraints =  false
        
        
        addSubview(customBackgroundView)
        addSubview(messageLabel)
        addSubview(dateLabel)
        
        let constraints = [
            messageLabel.topAnchor.constraint(equalTo: topAnchor, constant: 16),
            messageLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -32),
            messageLabel.widthAnchor.constraint(lessThanOrEqualToConstant: 250),
            
            dateLabel.topAnchor.constraint(equalTo: customBackgroundView.topAnchor, constant: 32),
            dateLabel.widthAnchor.constraint(lessThanOrEqualToConstant: 50),
            dateLabel.bottomAnchor.constraint(equalTo: customBackgroundView.bottomAnchor, constant: 32),
            
            customBackgroundView.topAnchor.constraint(equalTo: messageLabel.topAnchor, constant: -16),
            customBackgroundView.bottomAnchor.constraint(equalTo: messageLabel.bottomAnchor, constant: 16),
            customBackgroundView.leadingAnchor.constraint(equalTo: messageLabel.leadingAnchor, constant: -16),
            customBackgroundView.trailingAnchor.constraint(equalTo: messageLabel.trailingAnchor, constant: 16),
            ]
        NSLayoutConstraint.activate(constraints)
        
        leadingConstraint = messageLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 32)
        leadingConstraint.isActive = false
        
        trailingConstraint = messageLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -32)
        trailingConstraint.isActive = true
        
        
        dateLeadingConstraint = dateLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 18)
        dateLeadingConstraint.isActive = false
        
        dateTrailingConstraint = dateLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -18)
        dateTrailingConstraint.isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
