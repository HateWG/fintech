//
//  ConversationDataProvider.swift
//  FirstHW
//
//  Created by Томас Димеджи Акинделе Ало on 19/11/2018.
//  Copyright © 2018 Tinkoff Fintech. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class ConversationDataProvider : NSObject {
    
    let fetchedResultsController: NSFetchedResultsController<Message>
    let tableView: UITableView
    
    init(tableView: UITableView, conversationId: String) {
        self.tableView = tableView
        
        let fetchRequest = NSFetchRequest<Message>(entityName: "Message")
        
        let predicate = NSPredicate(format: "conversation.id == %@", conversationId)
        fetchRequest.predicate = predicate
        
        
        let sortByTimestamp = NSSortDescriptor(key: "date",ascending: true)
        fetchRequest.sortDescriptors = [sortByTimestamp]
        
        self.fetchedResultsController = NSFetchedResultsController<Message>(fetchRequest: fetchRequest,
                                                                            managedObjectContext: CoreDataStack.instance.mainContext,
                                                                    sectionNameKeyPath: #keyPath(Message.day),
                                                                            cacheName: nil)
        
        super.init()
        self.fetchedResultsController.delegate = self
    }
}

extension ConversationDataProvider : NSFetchedResultsControllerDelegate {
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
        case .insert:
            tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        case .move:
            break
        case .update:
            tableView.reloadSections(IndexSet(integer: sectionIndex), with: .fade)
            break
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .automatic)
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .automatic)
        case .update:
            tableView.reloadRows(at: [indexPath!], with: .automatic)
        case .move:
            tableView.insertRows(at: [newIndexPath!], with: .automatic)
            tableView.deleteRows(at: [indexPath!], with: .automatic)
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.endUpdates()
    }
}
