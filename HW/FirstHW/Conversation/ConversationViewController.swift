//
//  ConversationViewController.swift
//  FirstHW
//
//  Created by Томас Димеджи Акинделе Ало on 08/10/2018.
//  Copyright © 2018 Tinkoff Fintech. All rights reserved.
//

import UIKit
import CoreData

class ConversationViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    
    @IBOutlet weak var conversationTableView: UITableView!
    @IBOutlet weak var sendMessageTextField: UITextField!
    @IBOutlet weak var sendViewBottomConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var sendMessageButton: UIButton!
    
    
    var communicationManager : CommunicatorManager?
    var fetchedResultsController : NSFetchedResultsController<Message>?
    var conversationProvider : ConversationDataProvider?
    var conversation: ConversationsListCell?
    
    var userId : String?
    var userStatus : Bool = false
    
    fileprivate let cellId = "cell1"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        communicationManager?.userState = self
        
        self.conversationProvider = ConversationDataProvider(tableView: self.conversationTableView, conversationId: self.userId!)
        self.fetchedResultsController = self.conversationProvider?.fetchedResultsController
        do {
            try self.fetchedResultsController?.performFetch()
        } catch {
            print("Error fetching: \(error)")
        }
        
        conversationTableView.dataSource = self
        conversationTableView.rowHeight = UITableView.automaticDimension
        conversationTableView.estimatedRowHeight = 44
        conversationTableView.separatorStyle = .none
        
        sendMessageButton.isEnabled = false
        
//        navigationController?.navigationBar.prefersLargeTitles = true
        setTitleView()
        statusOnline()
//        title = userName
        
        conversationTableView.register(ChatMessageCell.self, forCellReuseIdentifier: cellId)
        conversationTableView.separatorStyle = .none
        
        let tapView = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tapView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        addKeyboardObservers()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        removeKeyboardObservers()
    }
    
    @objc func dismissKeyboard() {
        sendMessageTextField.endEditing(true)
    }
    
    @IBAction func messageTextFieldChange(_ sender: Any) {
        if (sendMessageTextField.text != "") && (userStatus == true){

//            sendMessageButton.isEnabled = true
            self.animationOfButton(button: self.sendMessageButton, isEnable: true)
        } else {
            self.animationOfButton(button: self.sendMessageButton, isEnable: false)
//            sendMessageButton.isEnabled = false
        }
    }
    
    @IBAction func sendMessageButtonAction(_ sender: Any) {
        guard let messageText = self.sendMessageTextField.text else {
            return
        }
        
        self.communicationManager?.sendMessage(text: messageText, toUser: self.userId!, completion: { (success, error) in
            if success {
                DispatchQueue.main.async {
                    self.sendMessageTextField.text = nil
                    self.sendMessageButton.isEnabled = false
                }
            }
        })
    }
    
    
    func setTitleView(){
        let label = UILabel()
        label.textColor = UIColor.black
        label.textAlignment = .center
        label.font = UIFont(name: "Helvetica-Bold", size: 17.0)
        label.adjustsFontSizeToFitWidth = true
        label.text = title
        navigationItem.titleView = label
    }
    func animationOfTitle(isOnline: Bool, label: UILabel){
        DispatchQueue.main.async {
        if isOnline {
            UIView.animate(withDuration: 1) {
                label.textColor = UIColor.green
                label.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
            }
        } else {
            UIView.animate(withDuration: 1) {
                label.textColor = UIColor.black
                label.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            }
            }
        }
    }
    
    func animationOfButton(button: UIButton, isEnable: Bool) {
        DispatchQueue.main.async {
        if isEnable {
        UIView.animate(withDuration: 0.5, animations: {
            button.isEnabled = isEnable
            button.transform = CGAffineTransform(scaleX: 1.15, y: 1.15)
        }) { (completed) in
            UIView.animate(withDuration: 0.5){
                button.transform = CGAffineTransform(scaleX: 1, y: 1)
            }
        }
        }else {
            button.isEnabled = isEnable
        }
        }
    }
    
    func statusOnline() {
        let label = navigationItem.titleView
        if userStatus{
            animationOfTitle(isOnline: userStatus, label: label as! UILabel)
            print("___________ONLINE")
            
        }else {
            animationOfTitle(isOnline: userStatus, label: label as! UILabel)
            print("___________OFFONLINE")
        }
    }
    
    
    private func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> String? {
        guard let fetchedResults = fetchedResultsController,
            let sections = fetchedResults.sections
            else {
                return "Date"
        }
        return sections[section].indexTitle!
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let sections = fetchedResultsController?.sections else {
            return 1
        }
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sections = fetchedResultsController?.sections
        else {
            return 0
        }
        return sections[section].numberOfObjects
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = conversationTableView.dequeueReusableCell(withIdentifier: cellId) as? ChatMessageCell
        let message = self.fetchedResultsController?.object(at: indexPath)
        
        cell?.configCell(message: message!)
        
        return cell ?? UITableViewCell()
    }
    
}

extension ConversationViewController: KeyboardAvoidable {
    var layoutConstraintsToAdjust: NSLayoutConstraint! {
        return sendViewBottomConstraint
    }
}

extension ConversationViewController: UserState {
    func becomeOnline(userId: String) {
        let label = navigationItem.titleView
        userStatus = true
        animationOfTitle(isOnline: true, label: label as! UILabel)
//        DispatchQueue.main.async {
//            if self.sendMessageTextField.text == nil {
//              self.animationOfButton(button: self.sendMessageButton, isEnable: false)
//        }
//        }
//        print(self.sendMessageTextField.text)
         DispatchQueue.main.async {
        if self.sendMessageTextField.text == ""{
            print("text is nil")
        }else{
            print("text not nil")
            self.animationOfButton(button: self.sendMessageButton, isEnable: true)
        }
        }
        print("___________ONLINE")
    }
    
    func becomeOffline(userId: String) {
        let label = navigationItem.titleView
        userStatus = false
        animationOfTitle(isOnline: false, label: label as! UILabel)
//        animationOfButton(button: sendMessageButton, isEnable: false)
        print("___________OFFONLINE")
        self.animationOfButton(button: self.sendMessageButton, isEnable: false)
//        print(sendMessageTextField.text)
//        DispatchQueue.main.async {
//            if self.sendMessageTextField.text == nil{
//                self.sendMessageTextField.text = nil
//            }
//        }
    }
    
    
    
}
