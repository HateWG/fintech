//
//  ConversationsListCell.swift
//  FirstHW
//
//  Created by Томас Димеджи Акинделе Ало on 08/10/2018.
//  Copyright © 2018 Tinkoff Fintech. All rights reserved.
//

import UIKit

class ConversationsListCell: UITableViewCell, UITableViewDelegate, conversationCellConfiguration {
    
    var peerId: NSObject?
    var userId: String?
    
    
    @IBOutlet weak var nameLabel: UILabel!
    var name: String? {
        didSet {
            if name != nil {
                nameLabel.text = name
            }else {
                nameLabel.text = "No name"
            }
        }
    }
    
    
    @IBOutlet weak var messageLabel: UILabel!
    var lastMessageText: String? {
        didSet {
            messageLabel.numberOfLines = 3
            messageLabel.font = UIFont.init(name: "System", size: 17)
            messageLabel.textColor = UIColor.black
            
            if lastMessageText != nil{
                messageLabel.text = lastMessageText
            }else {
                messageLabel.textColor = UIColor.lightGray
                messageLabel.font = UIFont.italicSystemFont(ofSize: 17)
                messageLabel.text = "No messages yet"
            }
        }
    }
    
    
    @IBOutlet weak var dateLabel: UILabel!
    var lastMessageDate: Date?{
        didSet {
            guard let date = self.lastMessageDate else {
                dateLabel.text = nil
                return
            }
            if lastMessageText != nil{
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "HH:mm"
                dateLabel.text = dateFormatter.string(from: date)
                
            }
        }
    }
    
    
    
    var online: Bool? {
        didSet {
            if online == true {
                contentView.backgroundColor = UIColor(red:0.98, green:0.95, blue:0.63, alpha:1.0)
            }else {
                contentView.backgroundColor = UIColor.clear
            }
        }
    }
    
    var hasUnreadMesseges: Bool? {
        didSet {
            if (hasUnreadMesseges == true) && (lastMessageText != nil) {
                messageLabel.textColor = UIColor.black
                messageLabel.font = UIFont.boldSystemFont(ofSize: 17)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
