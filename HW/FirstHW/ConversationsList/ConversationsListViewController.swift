//
//  ConversationsListViewController.swift
//  FirstHW
//
//  Created by Томас Димеджи Акинделе Ало on 03/10/2018.
//  Copyright © 2018 Tinkoff Fintech. All rights reserved.
//

import UIKit
import Foundation
import CoreData


class ConversationsListViewController: UITableViewController{

    @IBOutlet weak var profbut: UIBarButtonItem!
    
    let cellId = "cell"
    
    
    private var headerTitles = ["Online", "History"]
    
    let storageManager : StorageManage = StorageManage()
    var communicationManager : CommunicatorManager?
    var fetchedResultsController: NSFetchedResultsController<User>?
    var conversationsProvider : ConversationsDataProvider?
    
//    private var emitter = Emitter!
//    var window: UIWindow?
    
    override func viewDidLoad() {
        super.viewDidLoad()
         self.communicationManager = CommunicatorManager(storage: self.storageManager)
        
        self.conversationsProvider = ConversationsDataProvider(tableView: self.tableView)
        self.fetchedResultsController = self.conversationsProvider?.fetchedResultsController
        tableView.dataSource = self
        tableView.delegate = self

        navigationItem.title = "Tinkoff Chat"
        
        do {
            try self.fetchedResultsController?.performFetch()
        } catch {
            print(error)
        }
//        view.isUserInteractionEnabled  = true
//        self.window = EmittingWindow(frame: UIScreen.main.bounds)
//        self.window?.rootViewController = self
//        window?.makeKeyAndVisible()
//        addPanGesture(view: window! )
        
//        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handlePan))
//        view.addGestureRecognizer(tap)
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let rowsCount = self.fetchedResultsController?.sections?[section].numberOfObjects {
            return rowsCount
        }
        
        return 0
        
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        if let sectionsCount = self.fetchedResultsController?.sections?.count {
            return sectionsCount
        }
        
        return 2
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard let fetchedResults = fetchedResultsController,
            let sections = fetchedResults.sections
        else {
            return nil
        }
        return sections[section].indexTitle == "1" ? "Online" : "History"
        
    }
    
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! ConversationsListCell

        let user = self.fetchedResultsController?.object(at: indexPath)
        cell.name = user?.name
        cell.online = user?.isOnline
        cell.lastMessageText = user?.lastMessage?.text
        cell.lastMessageDate = user?.lastMessage?.date
        cell.hasUnreadMesseges = user?.lastMessage?.isUnread
        cell.userId = user?.id

        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let selectedCell = sender as? ConversationsListCell {
            if let selectedIndexPath = self.tableView.indexPath(for: selectedCell) {
                self.tableView.deselectRow(at: selectedIndexPath, animated: true)
                let conversationViewController = segue.destination as? ConversationViewController
                conversationViewController?.title = selectedCell.name
                conversationViewController?.communicationManager = self.communicationManager
                conversationViewController?.userId = selectedCell.userId
                conversationViewController?.userStatus = selectedCell.online ?? false
            }
        }
    }
    
    
//    func test() {
    
//        let emitter: Caem = Emitter.get(with: UIImage.init(named: "sendMessageLogo")! )
//        emitter.position = CGPoint(x: 55, y: 60)
//        view.layer.addSublayer(emitter)
        
//    }
//    func addPanGesture(view: UIView) {
//
//        let pan = UIPanGestureRecognizer(target: self, action: #selector(self.handlePan(sender:)))
//        view.addGestureRecognizer(pan)
//        pan.delaysTouchesBegan = true
//    }
    
//    @objc func handlePan(sender: UIPanGestureRecognizer) {
    
        
//        let emitter = TinEmitter()
//        let fileView = sender.view!
//        let location = sender.location(in: view)
        
//        view.layer.addSublayer(emitter)
//        emitter.renderMode = CAEmitterLayerRenderMode.additive
//        switch sender.state {
//
//        case .began:
//            emitter.startEmitting(with: sender.)
//            emitter.position = location
//            emitter.birthRate = 1
//            moveViewWithPan(emitter: emitter, location: location)
            
//            print("start and began")
//            print(location.x)
//            print(location.y)
            
//        case .changed:
//            emitter.updateEmittingPosition(with: sender)
//            updateEmittingPosition(location: location)
//            print("change")
        
//        case .cancelled, .ended:
//            emitter.stopEmitting()
//            stopEmitting(emitter: emitter)
//            print("End")
        
//            if fileView.frame.intersects(trashImageView.frame) {
//                deleteView(view: fileView)
//
//            } else {
//            returnViewToOrigin(view: sender.view!)
//            }
            
//        default:
//            break
//        }
//    }
    
//    func moveViewWithPan(emitter: CAEmitterLayer, location: CGPoint) {
//        let emitter = Emitter.get(with: UIImage.init(named: "sendMessageLogo")! )
//        emitter.birthRate = 1
//        emitter.position = CGPoint(x: location.x , y: location.y)
//        view.layer.addSublayer(emiter)
//        view.layer.layoutSublayers()
//    }
    
//    func updateEmittingPosition(emitter: CAEmitterLayer, location: CGPoint) {
    
//        let emitter = Emitter.get(with: UIImage.init(named: "sendMessageLogo")! )
//        emitter.position = CGPoint(x: location.x , y: location.y)
//        view.layer.addSublayer(emitter)
//    }
    
//    func stopEmitting(emitter: CAEmitterLayer) {
//        emitter.birthRate = 0
//        emitter.layoutSublayers()
//    }
//    func returnViewToOrigin(view: UIView) {
//
//        UIView.animate(withDuration: 0.3, animations: {
//            view.frame.origin = self.fileViewOrigin
//        })
//    }
}
//extension ConversationsListViewController: ContactsDelegate {
//    func contactListUpdated() {
//        DispatchQueue.main.async {
//            self.tableView.reloadData()
//        }
//}
//}
