//
//  TinkoffEmblemAnimation.swift
//  FirstHW
//
//  Created by Томас Димеджи Акинделе Ало on 30/11/2018.
//  Copyright © 2018 Tinkoff Fintech. All rights reserved.
//

import Foundation
import UIKit



class TinEmitter {
    
    lazy var emitterLayer: CAEmitterLayer = {
        let emitterLayer = CAEmitterLayer()

        emitterLayer.emitterCells = generateEmitterCells()
        emitterLayer.zPosition = CGFloat(Float.greatestFiniteMagnitude)
        emitterLayer.birthRate = 0
        return emitterLayer
    }()
    
    private weak var view: UIView?
    
    init(_ view: UIView) {
        self.view = view
        view.layer.addSublayer(emitterLayer)
    }
    
    func generateEmitterCells() -> [CAEmitterCell] {
        
        var cells:[CAEmitterCell] = [CAEmitterCell]()
        let cell = CAEmitterCell()
        cell.contents = #imageLiteral(resourceName: "logo").cgImage
        cell.emissionRange = 500
        cell.alphaSpeed = 1
        cell.birthRate = 5
        cell.lifetime = 0.5
        cell.velocity = 100
        cell.velocityRange = 50
        cell.spin = 3
        cell.scale = 0.09
        cell.scaleRange = 0.001
        
        cells.append(cell)
        
        return cells
    }
}
