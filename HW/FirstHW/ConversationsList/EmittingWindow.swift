//
//  emittingWindow.swift
//  FirstHW
//
//  Created by Томас Димеджи Акинделе Ало on 02/12/2018.
//  Copyright © 2018 Tinkoff Fintech. All rights reserved.
//

import Foundation
import UIKit

class EmittingWindow: UIWindow {
    
    private var emitter: TinEmitter!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        emitter = TinEmitter(self)
//        addPanGesture(view: self )
//        win(view: self)

    }
    
//    func win (view: UIWindow){
//        let pan = UIPanGestureRecognizer(target: self, action: #selector(handlePan(sender:)))
//        view.addGestureRecognizer(pan)
//        print("winddd")
//        pan.delegate = self
//        pan.cancelsTouchesInView = false
//        pan.delaysTouchesBegan = true
//        pan.delaysTouchesEnded = false
//    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
        
    }
    
//    func addPanGesture(view: UIWindow) {
//
//        let pan = UITapGestureRecognizer(target: self, action: #selector(self.handlePan(sender:)))
//        view.addGestureRecognizer(pan)
//        print("addPAN")
//    }
    
    func touchesBegan(_ touches: Set<UITouch>) {
        emitter.emitterLayer.birthRate = 1
        if let touch = touches.first {
            
            let location = touch.location(in: window)
            emitter.emitterLayer.emitterPosition = location
        }
    }
    
    func touchesMoved(_ touches: Set<UITouch>) {
        if let touch = touches.first {
            
            let location = touch.location(in: window)
            emitter.emitterLayer.emitterPosition = location
        }
    }
    
    func touchesEnded() {
        emitter.emitterLayer.birthRate = 0
    }
    
    override func sendEvent(_ event: UIEvent) {

        if let touches = event.allTouches, let touch = touches.first {
        switch touch.phase {
            
        case .began:
            touchesBegan(touches)

        case .moved:
            touchesMoved(touches)

        case .ended:
            touchesEnded()

        default:
            break
        }
    }
        super.sendEvent(event)
    }

}
//extension emittingWindow: UIGestureRecognizerDelegate {
//
//    @objc func handlePan(sender: UITapGestureRecognizer) {
//
//        switch sender.state {
//
//        case .began:
//            print("!!!start and begann")
//            logoEmitter.startEmitting(with: sender)
//        case .changed:
//            print("!!!!changee")
//            logoEmitter.updateEmittingPosition(with: sender)
//
//        case .cancelled, .ended:
//            logoEmitter.stopEmitting()
//            print("!!!!Endd")
//
//        default:
//            break
//        }
//    }
//
//}
//import UIKit.UIGestureRecognizerSubclass
//
//class myRecon: UIGestureRecognizer {
//
//}
