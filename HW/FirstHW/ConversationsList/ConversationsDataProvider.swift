//
//  ConversationsDataProvider.swift
//  FirstHW
//
//  Created by Томас Димеджи Акинделе Ало on 19/11/2018.
//  Copyright © 2018 Tinkoff Fintech. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class ConversationsDataProvider : NSObject {
    
    let fetchedResultsController : NSFetchedResultsController<User>
    
    let tableView : UITableView
    
    init(tableView: UITableView) {
        self.tableView = tableView
        let fetchRequest = NSFetchRequest<User>(entityName: "User")
        fetchRequest.sortDescriptors = [
            NSSortDescriptor(key: "isOnline", ascending: false),
            NSSortDescriptor(key: "lastMessage.date", ascending: false),
            NSSortDescriptor(key: "name", ascending: true)
        ]
        
        self.fetchedResultsController = NSFetchedResultsController<User>(fetchRequest: fetchRequest,
                                                                                 managedObjectContext: CoreDataStack.instance.mainContext, sectionNameKeyPath: #keyPath(User.isOnline),
                                                                                 cacheName: nil)
        
        super.init()
        self.fetchedResultsController.delegate = self
    }
}

extension ConversationsDataProvider : NSFetchedResultsControllerDelegate {
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
    switch type {
    case .insert:
    tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
    case .delete:
    tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
    case .move:
    break
    case .update:
    tableView.reloadSections(IndexSet(integer: sectionIndex), with: .fade)
    break
    }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .automatic)
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .automatic)
        case .update:
            tableView.reloadRows(at: [indexPath!], with: .automatic)
        case .move:
            tableView.insertRows(at: [newIndexPath!], with: .automatic)
            tableView.deleteRows(at: [indexPath!], with: .automatic)
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.endUpdates()
    }
    
}
