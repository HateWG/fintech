//
//  conversationCellConfiguration.swift
//  FirstHW
//
//  Created by Томас Димеджи Акинделе Ало on 04/10/2018.
//  Copyright © 2018 Tinkoff Fintech. All rights reserved.
//

import Foundation
import UIKit

protocol conversationCellConfiguration: class {
    var name: String? {get set}
    var lastMessageText: String? { get }
    var lastMessageDate: Date? { get }
    var online: Bool? {get}
    var hasUnreadMesseges: Bool? {get}
}
