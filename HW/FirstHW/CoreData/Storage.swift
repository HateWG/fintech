//
//  StorageManager.swift
//  FirstHW
//
//  Created by Томас Димеджи Акинделе Ало on 09/11/2018.
//  Copyright © 2018 Tinkoff Fintech. All rights reserved.
//

import Foundation
import UIKit
import CoreData

enum result {
    case success
    case fail
}

class StorageManage{
    weak var contactsDelegate: ContactsDelegate?
    //    -----------------------------------------------------------------------------------------------------
    func appUser(context: NSManagedObjectContext) throws -> AppUser {
        let request = NSFetchRequest<AppUser>(entityName: "AppUser")
        let users = try context.fetch(request)
        if let user = users.first {
            return user
        }
        let user = NSEntityDescription.insertNewObject(forEntityName: "AppUser", into: context)
            as! AppUser
        return user
    }
    
    func saveProfile(profile: ProfileModel, completion: @escaping (result) -> ()) {
        let context = CoreDataStack.instance.saveContext
        do {
            let aUser = try appUser(context: context)
            aUser.name = profile.profileName
            
            aUser.userDescription = profile.description
            
            aUser.image = profile.profileImage
            
            context.perform {
                CoreDataStack.instance.performSave(context: context) {
                    DispatchQueue.main.async {
                        completion(.success)
                    }
                }
            }
            
        }catch {
            print("error with connection to database")
            completion(.fail)
        }
        
    }
    
    func loadProfile(completion: @escaping (ProfileModel?) -> ()) {
        let context = CoreDataStack.instance.mainContext
        do {
            let aUser = try appUser(context: context)
            if aUser.name == nil && aUser.userDescription == nil && aUser.image == nil {
                completion(nil)
            } else {
                let profileModel = ProfileModel(profileName: aUser.name, description: aUser.userDescription, profileImage: aUser.image as! UIImage)
                
                DispatchQueue.main.async {
                    completion(profileModel)
                }
            }
        }catch {
            print("error with connection to database")
        }
}

}

extension StorageManage: StorageManagerProtocol{
    
    func recieveMessage(text: String, fromUser: String) {
        let context = CoreDataStack.instance.saveContext
        context.perform {
            let message = NSEntityDescription.insertNewObject(forEntityName: "Message", into: context) as? Message
            if let foundConversation = User.findUser(with: fromUser, in: context) {
                message?.isUnread = true
                message?.isIncoming = true
                message?.text = text
                message?.date = Date()
                message?.conversation = foundConversation
                message?.lastMessage = foundConversation
                } else {
                    let conversation = User.insertUser(with: fromUser, in: context)
                    conversation?.id = fromUser
                    message?.conversation = conversation
                    message?.lastMessage = conversation
                }
            CoreDataStack.instance.performSave(context: context, completionHandler: nil)
            }
        
        }
    
    func sendMessage(text: String, to userId: String) {
        let context = CoreDataStack.instance.saveContext
        context.perform {
            let message = NSEntityDescription.insertNewObject(forEntityName: "Message", into: context) as? Message
            if let foundConversation = User.findUser(with: userId, in: context) {
                message?.isIncoming = false
                message?.date = Date()
                message?.text = text
                message?.isUnread = false
                message?.messageId = userId
                message?.conversation = foundConversation
                message?.lastMessage = foundConversation
                } else {
                    let conversation = User.insertUser(with: userId, in: context)
                    conversation?.id = userId
                    message?.conversation = conversation
                    message?.lastMessage = conversation
                }
            CoreDataStack.instance.performSave(context: context, completionHandler: nil)
        }
//        contactsDelegate?.contactListUpdated()
    }
    func didFoundUser(userID: String, userName: String?) {
        let context = CoreDataStack.instance.saveContext
        
        if let user = User.findUser(with: userID, in: context){
            print("#######NASHLI#########")
//            contactsDelegate?.contactListUpdated()
            user.name = userName
            user.id = userID
            user.isOnline = true
        }else {
            print("#######СОЗДАЛИ НОВОГО#########")
            contactsDelegate?.contactListUpdated()
            let user = User.insertUser(with: userID, in: context)
            user?.name = userName
            user?.id = userID
            user?.isOnline = true
        }
        CoreDataStack.instance.performSave(context: context, completionHandler: nil)
    }
    
    func didLostUser(userID: String) {
        let context = CoreDataStack.instance.saveContext
        let user = User.findUser(with: userID, in: context)
        user?.isOnline = false
        CoreDataStack.instance.performSave(context: context, completionHandler: nil)
//        contactsDelegate?.contactListUpdated()
    }
}

extension User {
    
    static func insertUser(with id: String, in context:NSManagedObjectContext) -> User? {
        if let user = NSEntityDescription.insertNewObject(forEntityName: "User", into: context) as? User {
            user.id = id
            return user
        }
        
        return nil
    }
    
    static func findUser(with id: String, in context: NSManagedObjectContext) -> User? {
        guard let model = context.persistentStoreCoordinator?.managedObjectModel else {
            print ("Model is not available in context")
            assert(false)
            return nil
        }
        
        var user : User?
        guard let fetchRequest = User.fetchRequestUser(with: id, model: model) else {
            return nil
        }
        
        do {
            let results = try context.fetch(fetchRequest)
            assert(results.count < 2, "Multiple Conversations found!")
            if let foundUser = results.first{
                user = foundUser
            }
        } catch {
            print ("Failed to fetch conversation: \(error)")
        }
        
        return user
    }
    
    static func fetchRequestUser(with id: String, model: NSManagedObjectModel) -> NSFetchRequest<User>? {
        let templateName = "UserById"
        
        guard let fetchRequest = model.fetchRequestFromTemplate(withName: templateName, substitutionVariables: ["id" : id]) as? NSFetchRequest<User> else{
            assert(false,"No template with name \(templateName)")
            return nil
        }
        
        return fetchRequest
    }

}

extension Message {
    
    @objc var day: String? {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM d - EEEE"
        return date.map(formatter.string(from:))
    }
}

