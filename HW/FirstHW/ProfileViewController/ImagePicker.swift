//
//  ImagePicker.swift
//  FirstHW
//
//  Created by Томас Димеджи Акинделе Ало on 31/10/2018.
//  Copyright © 2018 Tinkoff Fintech. All rights reserved.
//

import UIKit
import Foundation

protocol ImagePickerDelegate: class {
    func picker(_ picker: ImagePicker, image: UIImage?)
}
final  class ImagePicker: UIImagePickerController {
    weak var imagePickerDelegate: ImagePickerDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
    }
}

extension ImagePicker: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        imagePickerDelegate?.picker(self, image: image)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        imagePickerDelegate?.picker(self, image: nil)
    }
}

private var completionBlock: ((UIImage?) -> Void)?

protocol imageSourcePicker: ImagePickerDelegate {
    func pick(completion: @escaping (UIImage?) -> Void)
}

extension imageSourcePicker where Self: UIViewController {
    
    func pick(completion: @escaping (UIImage?) -> Void) {
        
        completionBlock = completion
        let imagePickerController = ImagePicker()
        imagePickerController.imagePickerDelegate = self
        
        
        let alertController = UIAlertController(title: "Изменить фото", message: nil, preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: "Камера", style: .default) { (action) in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePickerController.sourceType = .camera
                self.present(imagePickerController, animated: true, completion: nil)
            }else {
                print("camera not available")
                
                let alertController = UIAlertController(title: "Ошибка", message: "Камера недоступна", preferredStyle: .alert)
                
                alertController.addAction(UIAlertAction(title: "OK", style: .cancel) { (action:UIAlertAction) in
                })
                self.present(alertController, animated: true, completion: nil)
            }
            
        }
        
        let photosGalleryAction = UIAlertAction(title: "Галерея", style: .default) { (action) in
            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)
        }
        
        let savedPhotosAction = UIAlertAction(title: "Сохраненные Фото", style: .default) { (action) in
            imagePickerController.sourceType = .savedPhotosAlbum
            self.present(imagePickerController, animated: true, completion: nil)
        }
        
//        let internetImagesViewController = self.presentationAssembly.internetImagesViewController()
//        internetImagesViewController.delegate = self
//        let navigationController = self.presentationAssembly.navigationController(rootViewController: internetImagesViewController)
//        self.present(navigationController, animated: true, completion: nil)
//
        let loadPhotosAction = UIAlertAction(title: "Загрузить", style: .default) { (action) in
            
            guard let imageSearchVC = self.storyboard?.instantiateViewController(withIdentifier: "PictureCollectionViewController") as? PictureCollectionViewController else {
                print("neydacha")
                return
            }
            imageSearchVC.delegate = self  as? PictureLoaderViewControllerDelegate
            self.present(imageSearchVC, animated: true)
        }
        
        let cancelAction = UIAlertAction(title: "Отменить", style: .cancel, handler: nil)
        
        //        let removeImageAction = UIAlertAction(title: "Удалить фото", style: .destructive) { (action) in
        //            self.profileImg.image = UIImage(named: "placeholderuser")
//                    self.setButtonsAble()
        
        
        alertController.addAction(cameraAction)
        alertController.addAction(photosGalleryAction)
        alertController.addAction(savedPhotosAction)
        alertController.addAction(loadPhotosAction)
        alertController.addAction(cancelAction)
        //        alertController.addAction(removeImageAction)
        
        present(alertController, animated: true, completion: nil)
        self.present(imagePickerController, animated: true, completion: nil)
        
        
    }
    
    func picker(_ viewController: ImagePicker, image: UIImage?) {
        
        completionBlock?(image)
        completionBlock = nil
        viewController.dismiss(animated: true, completion: nil)
    }
    
}
