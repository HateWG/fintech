//
//  Model.swift
//  FirstHW
//
//  Created by Томас Димеджи Акинделе Ало on 19/10/2018.
//  Copyright © 2018 Tinkoff Fintech. All rights reserved.
//

import Foundation
import UIKit

class ProfileModel {

    
    var profileName: String?
    var description: String?
    var profileImage: UIImage?
    
    init() {
        
    }
    
    init(profileName: String?, description: String?, profileImage: UIImage ) {
        self.profileName = profileName
        self.description = description
        self.profileImage = profileImage
    }
    
}
