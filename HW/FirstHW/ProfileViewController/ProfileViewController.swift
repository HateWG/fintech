//
//  ProfileViewController.swift
//  FirstHW
//
//  Created by Томас Димеджи Акинделе Ало on 26/09/2018.
//  Copyright © 2018 Tinkoff Fintech. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController, UITextViewDelegate, UITextFieldDelegate, imageSourcePicker {
    
    
    
    @IBOutlet weak var scrollViewBottomConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var profileName: UITextField!
    @IBOutlet weak var profileDescription: UITextView!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var choiceImgButton: UIButton!

    
    
    private var dataManager = StorageManage()
    private var profile: ProfileModel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //        Выводится ошибка "Fatal error: Unexpectedly found nil while unwrapping an Optional value" так как наша кнопка является опциональной величиной и в данный момент вывода она еще просто не создана и равна nil
        if editButton != nil {
            print("Кнопка существует")
            print("Frame = \(editButton.layer.frame)")
        } else {
            print("Кнопка еще не создана")
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        objStyle()
        self.title = "Профиль"
        readProfile()
        
        changeUIData(editMode: false)
        activityIndicator.hidesWhenStopped = true
//        profileImg.image = UIImage.init(named: "test")
        self.profileName.delegate = self
        self.profileDescription.delegate = self
        self.profile = ProfileModel(profileName: self.profileName.text, description: self.profileDescription.text, profileImage: self.profileImg.image!)
        
        let tapView = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tapView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addKeyboardObservers()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        removeKeyboardObservers()
    }
    
    @objc func dismissKeyboard() {
        profileName.endEditing(true)
        profileDescription.endEditing(true)
    }
    
    func objStyle() {
        profileImg.layer.cornerRadius = 20
        
        profileName.layer.cornerRadius = 5
        profileName.setLeftPaddingPoints(10)
        
        profileDescription.layer.cornerRadius = 15
        profileDescription.textContainerInset = UIEdgeInsets (top: 5, left: 8, bottom: 0, right: 0)
    }

//  ________________________________________________________________________________
    func changeUIData(editMode: Bool) {
        if editMode == true {

            editButton.isHidden = true
            saveButton.isHidden = false
            
            choiceImgButton.isHidden = false
            choiceImgButton.isEnabled = true
            choiceImgButton.backgroundColor = UIColor.clear
            
            setButtonsDisable()
            
            profileName.isUserInteractionEnabled = true
            profileDescription.isEditable = true
            
            profileName.backgroundColor = UIColor.clear
//            profileName.borderStyle = .roundedRect
            profileName.layer.borderWidth = 1
            
            profileDescription.backgroundColor = UIColor.clear
            profileDescription.layer.borderWidth = 1
            
            profileName.placeholder = "Enter your name:"
            
            if profileDescription.text == nil {
                profileDescription.text = "Write something about yourself"
                profileDescription.textColor = UIColor.lightGray
            }
            
            navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(stopEditing))
        } else {
            editButton.isHidden = false
            
            saveButton.isHidden = true
            choiceImgButton.isHidden = true
            
            setButtonsDisable()
            
            profileName.backgroundColor = UIColor.clear
            profileName.layer.borderWidth = 0
            
            profileDescription.backgroundColor = UIColor.clear
            profileDescription.layer.borderWidth = 0
            
            profileName.isUserInteractionEnabled = false
            profileDescription.isEditable = false
            
            navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(returnToConversationsList))
        }
    }
    
    @objc func returnToConversationsList() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func stopEditing() {
        changeUIData(editMode: false)
    }
    
    //    Состояния кнопки
    func setButtonsDisable() {
        self.saveButton.isEnabled = false
        self.saveButton.backgroundColor = UIColor.gray.withAlphaComponent(0.8)
        
    }
    func setButtonsAble() {
        self.saveButton.isEnabled = true
        self.saveButton.backgroundColor = UIColor.clear
    }
    
    //    placeholder для textView
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Write something about yourself"
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        setButtonsAble()
    }
    
    @IBAction func ProfileNameDidChange(_ sender: Any) {
        setButtonsAble()
    }
    //    _________________________________________________________
    
    @IBAction func editButtonAction(_ sender: UIButton) {
        changeUIData(editMode: true)
    }
    
    
    @IBAction func saveButtonAction(_ sender: Any) {
        choiceImgButton.isEnabled = false
        choiceImgButton.backgroundColor = UIColor.clear.withAlphaComponent(0.4)
        collectProfileData()
        saveProfile()
        
    }
    
    @IBAction func choiceImgButtonAction(_ sender: UIButton) {
  
            pick { image in
                if image != nil {
                    self.profileImg.image = image
                    self.setButtonsAble()
                }else{
                    self.setButtonsDisable()
                }
        }

    }
    
    private func collectProfileData() {
        profile = ProfileModel()
        profile.profileName = profileName.text
        profile.description = profileDescription.text
        if let image = profileImg.image {
            profile.profileImage = image
        }
    }
    
    private func saveProfile() {
        activityIndicator.startAnimating()
        
        dataManager.saveProfile(profile: profile) { result in
            switch result {
            case .success:
                print("Успешно сохранено")
                let successAlert = UIAlertController(title: nil, message: "Данные сохранены", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: { action in
                    successAlert.dismiss(animated: true, completion: nil)
                    self.activityIndicator.stopAnimating()
                    self.changeUIData(editMode: false)
                })
                successAlert.addAction(okAction)
                self.present(successAlert, animated: true, completion: nil)
                
            case .fail:
                print("Ошибка при сохранении")
                let failureAlert = UIAlertController(title: "Ошибка", message: "Не удалось сохранить данные", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: { action in
                    failureAlert.dismiss(animated: true, completion: nil)
                    self.activityIndicator.stopAnimating()
                    self.changeUIData(editMode: true)
                    self.setButtonsAble()
                })
                let retryAction = UIAlertAction(title: "Повторить", style: .default, handler: { action in
                    failureAlert.dismiss(animated: true, completion: nil)
                    self.saveProfile()
                })
                failureAlert.addAction(okAction)
                failureAlert.addAction(retryAction)
                self.present(failureAlert, animated: true, completion: nil)
            }
        }
    }
    
    private func readProfile() {
        
        self.dataManager.loadProfile(completion: {(profile) in
            if let profile = profile {
                self.profile = profile
                
                self.profileDescription.text = profile.description
            }
            self.profileImg.image = profile?.profileImage ?? UIImage.init(named: "placeholderuser")
            self.profileName.text = profile?.profileName ?? ""
            
        })
    }

}

extension ProfileViewController: KeyboardAvoidable {
    var layoutConstraintsToAdjust: NSLayoutConstraint! {
        return scrollViewBottomConstraint
    }   
}

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}

protocol ProfileViewControllerDelegate: class {
    
    func didSelectImageForProfile(image: UIImage)
}

extension ProfileViewController : PictureLoaderViewControllerDelegate {
    func imagePicked(image: UIImage) {
        if profileImg.image != image {
            profileImg.image = image
            setButtonsAble()
        }
    }
}
