//
//  CommunicatorManager.swift
//  FirstHW
//
//  Created by Томас Димеджи Акинделе Ало on 25/10/2018.
//  Copyright © 2018 Tinkoff Fintech. All rights reserved.
//

import Foundation
import CoreData

protocol ContactsDelegate: class {
    func contactListUpdated()
}

protocol StorageManagerProtocol: class {
    func recieveMessage(text: String, fromUser: String)
    func sendMessage(text: String, to userId: String)
    func didFoundUser(userID:String,userName:String?)
    func didLostUser(userID:String)
}

protocol UserState : class {
    
    func becomeOnline(userId: String)
    func becomeOffline(userId: String)
}



class CommunicatorManager {
    
    weak var contactsDelegate: ContactsDelegate?
    
    weak var userState: UserState?
    
    var communicator: Communicator
    
    var storage : StorageManagerProtocol

    
    init(storage : StorageManagerProtocol) {
        self.storage = storage
        self.communicator = MultipeerCommunicator()
        self.communicator.delegate = self
    }
}

extension CommunicatorManager: CommunicatorDelegate {
    
    func didFoundUser(userID: String, userName: String?) {
        self.storage.didFoundUser(userID: userID, userName: userName)
        contactsDelegate?.contactListUpdated()
        
         userState?.becomeOnline(userId: userID)
    }
    
    func didLostUser(userID: String) {
        self.storage.didLostUser(userID: userID)
        contactsDelegate?.contactListUpdated()
        
        userState?.becomeOffline(userId: userID)
    }
    
    func failedToStartBrowsingForUsers(error: Error) {
        
    }
    
    func failedToStartAdvertising(error: Error) {
        
    }
    
    func didRecievedMessage(text: String, fromUserId: String) {
        self.storage.recieveMessage(text: text, fromUser: fromUserId)
        contactsDelegate?.contactListUpdated()
    }
    
    func sendMessage(text: String, toUser: String, completion: ((Bool, Error?) -> ())?) {
        self.storage.sendMessage(text: text, to: toUser)
        self.communicator.sendMessage(string: text, to: toUser, completionHandler: completion)
        contactsDelegate?.contactListUpdated()
    }
}
